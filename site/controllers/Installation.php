<?php

namespace Controllers;

use Platypus\R;
use Plugins\Crypt;

class Installation extends DefaultController {
    public static function Index($req, $res, $extras = []) {
        parent::Index($req, $res, $extras);
    }

    public static function Save($req, $res, $extras = []) {
        try {
            $resp = new \stdclass();

            $config_dir = dirname(dirname(__DIR__)).'/config';

            foreach($_POST as $name => $value) {
                if (in_array($name, array('account'))) {
                    continue;
                }

                $data = $req->post($name, []);


                file_put_contents("{$config_dir}/{$name}.php", "<?php\n\nreturn ".var_export($data, true).';');
            }

            $account = $req->post('account',[]);

            if (!empty($account)) {
                $user = R::findOne('user', 'nivel = ?', array(0));
                if (!isset($user->id)) {
                    $user = R::dispense('user');
                }
                $user->nome = $account['nome'];
                $user->username = $account['username'];
                $user->email = $account['email'];
                $user->password = Crypt::hasher($account['password']);
                $user->nivel = 0;
                $user->ativo = 1;

                if (!R::store($user)) {
                    throw new \Exception("Não foi possível salvar a conta do usuário.");
                }
            }

            $resp->success = true;
            $resp->msg = "Dados salvos com sucesso.";
        } catch (\Exception $e) {
            $resp->success = false;
            $resp->msg = $e->getMessage();
            $resp->line = $e->getLine();
            $resp->file = $e->getFile();
        }

        $res->json($resp);
    }
}