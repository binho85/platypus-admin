<?php

$common = require __DIR__ . '/common.php';
$config = [
    'app' => [
        'path' => dirname(__DIR__) . '/site'
    ],
    'twig' => [
        'cache' => false
    ]
];

return array_merge_recursive($common, $config);