<?php

namespace Models\RedBean;

use Platypus\R;

class DefaultModel extends \RedBeanPHP\SimpleModel {
    public function open() {
    }
    public function dispense() {
        //$this->bean->created_at = date('Y-m-d H:i:s');
    }
    public function update() {
        $type = $this->bean->getMeta('type');
        if (preg_match('/_versions$/',$type)) {
            return;
        }
        $this->bean->setMeta('modificado', false);
        foreach($this->bean->export() as $key=>$value) {
            if ($this->bean->hasChanged($key) !== FALSE) {
                $this->bean->setMeta('modificado', true);
                break;
            }
        }
        if ($this->bean->getMeta('modificado')) {
            $this->bean->modified_at = date('Y-m-d H:i:s');
        }
    }
    public function after_update() {
        $type = $this->bean->getMeta('type');
        if (preg_match('/_versions$/',$type)) {
            return;
        }
        if (!$this->bean->getMeta('modificado')) {
            return;
        }

        $version = R::xdispense("{$type}_versions");

        foreach($this->bean->export() as $key => $value) {
            if (in_array($key, array('id'))) {
                continue;
            }

            $version->{$key} = $this->bean->old($key);
            $version->{$type.'_id'} = $this->bean->id;
            R::store($version);
        }

    }
    public function delete() {}
    public function after_delete() {}
}