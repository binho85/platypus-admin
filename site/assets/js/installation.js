const Installation = {
	exec(cmd, data) {
		return new Promise((resolve, reject) => {
			$.ajax({
				url: `installation/${cmd}`,
				type: "post",
				dataType: "json",
				data
			})
				.then(resp => {
					if (!resp.success) {
						reject(new Error(resp.msg));
						return;
					}
					resolve(resp);
				})
				.catch(err => {
					reject(
						new Error("Não foi possível completar a solicitação.")
					);
				});
		});
	},
	save(data) {
		return this.exec("save", data);
	}
};

$("body").on("submit", "[name=formInstallation]", function() {
	const $form = $(
		"[name=formApplication],[name=formDatabase],[name=formInstallation]"
	);

	let data = $form.serializeArray();
	Installation.save(data)
		.then(resp => {
			M.toast({
				classes: "green darken-4",
				html:
					'<i class="material-icons">check_circle</i> &nbsp;<span>' +
					(resp.msg || "Dados salvos com sucesso.") +
					"</span>",
				completeCallback: () => {
					location.href = document.baseURI + "home";
				}
			});
		})
		.catch(err => {
			M.toast({
				classes: "red darken-4",
				html:
					'<i class="material-icons">error</i> &nbsp;<span>' +
					(err.message || "Não foi possível salvar os dados.") +
					"</span>"
			});
		});

	return false;
});
var instance = M.Tabs.init(document.querySelector(".tabs"), {});
$("body").on("submit", "[name=formApplication]", function() {
	instance.select("database");
	return false;
});
$("body").on("submit", "[name=formDatabase]", function() {
	instance.select("account");
	return false;
});
