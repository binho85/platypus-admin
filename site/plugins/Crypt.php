<?php

namespace Plugins;

class Crypt {
    public static function hasher($plain, $salt = '') {
        if ($salt=='') $salt = '$1$'.substr(md5(uniqid()), 0,8).'$';
		return crypt($plain,$salt);
    }
}