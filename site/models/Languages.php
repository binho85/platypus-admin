<?php

namespace Models;

use Platypus\R;

class Languages {
    public static $records = null;

    public static function generate() {
        $rows = R::getAll("SELECT id, titulo, code FROM `language` WHERE ativo = 1");
        $records = array();
        foreach($rows as $row) {
            $row = (object)$row;
            $records[$row->code] = $row;
        }
        return $records;
    }

    public static function getAll() {
        if (null == static::$records) {
            static::$records = static::generate();
        }
        return static::$records;
    }

    public static function get($code) {
        $records = static::getAll();
        return isset($records[$code]) ? $records[$code] : null;
    }

    public static function getCodes() {
        $records = static::getAll();
        return array_keys($records);
    }
}