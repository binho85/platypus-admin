<?php



return [
    'db' => require(__DIR__.'/db.php'),
    'app' => require(__DIR__.'/app.php')
];