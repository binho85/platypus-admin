import babel from "rollup-plugin-babel";
export default [
	{
		plugins: [
			babel({
				presets: [["env", { modules: false }]],
				minified: true,
				comments: false
			})
		]
	}
];
