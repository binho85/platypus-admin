<?php

namespace Controllers;

use Platypus\R;

use Models\Produtos;

class Home extends DefaultController {
    public static function Index($req, $res, $extras = []) {



        parent::Index($req, $res, $extras);
    }

    public static function Create($req, $res, $extras = []) {

        $produto = R::dispense('produto');
        $produto->uuid = uniqid();
        $produto->ativo = 1;
        $produto->ordenamento = microtime(true);
        R::store($produto);

        $produtotranslation = R::dispense('produtotranslation');
        $produtotranslation->produto_uuid = $produto->uuid;
        $produtotranslation->language = 'pt-br';
        $produtotranslation->titulo = 'Título';
        R::store($produtotranslation);

        $produtotranslation = R::dispense('produtotranslation');
        $produtotranslation->produto_uuid = $produto->uuid;
        $produtotranslation->language = 'en-us';
        $produtotranslation->titulo = 'Title';
        R::store($produtotranslation);




        parent::Index($req, $res, $extras);
    }
}