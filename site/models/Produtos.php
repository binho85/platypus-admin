<?php

namespace Models;

use Platypus\R;

class Produtos {
    public static $records = null;
    public static function getAll() {
        if (null == static::$records) {
            static::$records = static::generate();
        }

        return static::$records;
    }

    public static function generate() {
        $rows = R::getAll("SELECT * FROM produto");
        $records = array();
        foreach($rows as $row) {
            $row = (object)$row;
            $row->translations = array();
            $records[$row->uuid] = $row;
        }

        if (!empty($records)) {
            $ids = array_keys($records);
            $ids = '"'.join('","', $ids).'"';

            $translations = R::getAll("SELECT * FROM produtotranslation WHERE produto_uuid in ({$ids})");
            foreach($translations as $translation) {
                $translation = (object)$translation;
                $records[$translation->produto_uuid]->translations[$translation->language] = $translation;
            }
        }

        foreach($records as $record) {
            $records[$record->uuid] = new Produto($record);
        }

        return $records;
    }

    public static function get($id) {
        $records = static::getAll();
        return isset($records[$id]) ? $record[$id] : null;
    }
}