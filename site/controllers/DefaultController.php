<?php

namespace Controllers;

use Platypus\Config;
use Platypus\Twig;

class DefaultController {
    public static function Index($req, $res, $extras = []) {
        $path = Config::get('app.path');
        $tpl = $req->controller . '/' . $req->action . '.twig';
        $filename = $path . '/views/' . $tpl;



        if (!file_exists( $filename )) {
            $tpl = "404/index.twig";
        }

        $defaultArgs = [
            'req' => $req,
            'config' => Config::$data
        ];

        $args = array_merge_recursive($defaultArgs, $extras);

        echo Twig::render($tpl, $args);
    }
}