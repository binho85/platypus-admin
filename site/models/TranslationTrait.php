<?php

namespace Models;

use Platypus\Http\Request;
use Platypus\Config;

trait TranslationTrait {
    public function __isset($key) {
        if (isset($this->record->{$key})) {
            return true;
        }

        $translation = $this->record->translations[Config::get('language')];
        if (isset($translation->{$key})) {
            return true;
        }

        return false;
    }

    public function __get($key) {
        if (isset($this->record->{$key})) {
            return $this->record->{$key};
        }

        $translation = $this->record->translations[Config::get('language')];
        if (isset($translation->{$key})) {
            return $translation->{$key};
        }

        return null;
    }

    public function __set($key, $value) {
        if (isset($this->record->{$key})) {
            $this->record->{$key} = $value;
        }

        $translation = $this->record->translations[Config::get('language')];
        if (isset($translation->{$key})) {
            $translation->{$key} = $value;
        }
    }
}