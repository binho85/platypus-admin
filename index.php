<?php

$loader = require __DIR__ . '/vendor/autoload.php';

use Platypus\App;
use Platypus\Config;

App::setup(__DIR__ . '/config/site.php', $loader);

Config::set("languages", \Models\Languages::getCodes());

App::start();